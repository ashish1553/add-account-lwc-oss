const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const cors = require('cors');

const jsforce = require('jsforce');
require('dotenv').config()

const { SF_LOGIN_URL, SF_USERNAME, SF_PASSWORD, SF_TOKEN } = process.env;

if (!(SF_USERNAME && SF_PASSWORD && SF_TOKEN && SF_LOGIN_URL)) {
    console.error(
        'Cannot start app: missing mandatory configuration. Check your .env file.'
    );
    process.exit(-1);
}

const conn = new jsforce.Connection({
    loginUrl: SF_LOGIN_URL
})

conn.login(SF_USERNAME, SF_PASSWORD + SF_TOKEN, (err, userInfo) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log("User Id: ", userInfo.id);
        console.log("Org Id: ", userInfo.organizationId);
        console.log("Url: ", userInfo.url);
    }
});

const app = express();
app.use(helmet());
app.use(compression());
app.use(cors());
app.use(express.json());

app.use((req, res, next) => {
    res.setHeader(
        'Content-Security-Policy',
        "default-src 'self'",
        "img-src 'self'",
        "script-src 'self' http://localhost:3001"
    )
    next()
})

const HOST = process.env.API_HOST || 'localhost';
const PORT = process.env.API_PORT || 3002;


app.get('/api/accounts', (req,res) => {
    const soql = `SELECT Id, Name, AccountNumber, Phone, Rating, Website FROM Account`;
    conn.query(soql, (err, result) => {
        console.log(err);
        console.log(result);
        if (err) {
            res.sendStatus(500);
            // return err
        } else {
            console.log("Total records: ", result.totalSize);
            res.send({ data: result.records });
        }
    })
})

app.post('/api/post/account', (req,res) => {
    console.log(req.body);
    conn.sobject("Account").create({ 
        Name : req.body.Name,
        AccountNumber: req.body.Number,
        Phone: req.body.Phone,
        Website: req.body.Website,
        Rating: req.body.Rating 
    }, function(err, ret) {
        if (err || !ret.success) { return console.error(err, ret); }
        res.json({ success: true, "message": "Account added successfully!", recordId: ret.id})
        return console.log("Created record id : " + ret.id);
    });
})


app.listen(PORT, () =>
    console.log(
        `✅  API Server started: http://${HOST}:${PORT}/api/v1/endpoint`
    )
);
