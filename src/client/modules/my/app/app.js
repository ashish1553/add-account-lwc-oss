import { LightningElement, track } from 'lwc';

export default class App extends LightningElement {

    @track currentPage = {
        accountsDatatable : true,
        createAccount: false
    }


    switchPage(event){

        Object.keys(this.currentPage).forEach(element => {
            this.currentPage[element] = false;
        });

        if (event.detail === 'createAccount') {
            this.currentPage.createAccount = true;
        } else if(event.detail === 'accountsDatatable') {
            this.currentPage.accountsDatatable = true;
        }

    }
}
