import { LightningElement } from 'lwc';
import { getAccounts } from 'data/accountData';
export default class AccountDataTable extends LightningElement {
    accounts = [];

    connectedCallback() {
        getAccounts().then(result => {
            this.accounts = result;
            console.log(result);
        });
        
    }

    createAccount(){
        this.dispatchEvent(new CustomEvent('pageswitch', {
            detail: 'createAccount'
        }));
    }

}