import { LightningElement, track } from 'lwc';

export default class CreateAccount extends LightningElement {

    @track showRatingPicklist = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
    ratingPicklistValue = '--None--';
    showSpinner = false;

    accountDetails = {};


    get ratingOptions(){
        return [
            {label: '--None--', value: 'none'},
            {label: 'Hot', value: 'hot'},
            {label: 'Warm', value: 'warm'},
            {label: 'Cold', value: 'cold'}
        ];
    }

    handleAccountName(event){
        this.accountDetails.Name = event.target.value;
    }

    handleAccountPhone(event){
        this.accountDetails.Phone = event.target.value;
    }

    handleAccountNumnber(event){
        this.accountDetails.Number = event.target.value;
    }

    handleAccountWebsite(event){
        this.accountDetails.Website = event.target.value;
    }

    handleRatingPicklist(){
        this.showRatingPicklist = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open";
    }

    handleRatingValue(event){
        this.showRatingPicklist = "slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click";
        this.ratingPicklistValue = event.currentTarget.dataset.label;
        this.accountDetails.Rating = event.currentTarget.dataset.label;
    }

    handleSaveAccount(){
        console.log(this.accountDetails);
        this.showSpinner = true;
        fetch('http://localhost:3001/api/post/account', {
            method: 'POST',
            headers: {
                'accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.accountDetails)
        }).then(response => {
            if(!response.ok){
                throw new Error('No Response From Server');
            }
            return response.json();
        }).then(result => {
            console.log("Account Created: ", result);
            this.showSpinner = false;
            this.dispatchEvent(new CustomEvent('pageswitch', {
                detail: 'accountsDatatable'
            }));
        }).catch(error => {
            console.error(error);
        })
    }
}